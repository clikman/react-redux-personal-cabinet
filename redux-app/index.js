import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { Router, Route, browserHistory, IndexRoute } from 'react-router'
import App from './redux-app'
import configureStore from './store'


const store = configureStore()

render(
  <Provider store={store}>
        <App />
  </Provider>,
  document.getElementById('cabinet')
)
