import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import User from './components/user'
import ObjectsPage from './components/objects_page'
import FilterFormContainer from './components/filter'
import FilterForm from './components/filter_form'
import Sender from './components/sender'
import * as userActions from './actions/user'
import * as objectsActions from './actions/objects'
import * as filterActions from './actions/filter'
import * as senderActions from './actions/sender'

class App extends Component {
  render() {
    const { user, objects, filter, sender, objects_choice, form } = this.props;
    const user_actions = this.props.userActions;
    const sender_actions = this.props.senderActions;
    const filter_actions = this.props.filterActions;
    const objects_actions = this.props.objectsActions;

    return <div className='row'>
        <FilterFormContainer filter_selected={filter.filter_selected} filter_actions={filter_actions} />
        <ObjectsPage objects_all={objects.objects_all} objects={objects.objects} objects_actions={objects_actions} objects_selected={objects_choice.ids} objects_prop={objects} />
        <Sender objects_selected={objects_choice.ids} sender_actions={sender_actions}  object_actions={objects_actions} />
    </div>
  }
}

function mapStateToProps(state) {
    return {
        user: state.user,
        objects: state.objects,
        filter: state.filter,
        sender: state.sender,
        objects_choice: state.objects_choice,
        form: state.form
    }
}

function mapDispatchToProps(dispatch) {
    return {
        userActions: bindActionCreators(userActions, dispatch),
        objectsActions: bindActionCreators(objectsActions, dispatch),
        filterActions: bindActionCreators(filterActions, dispatch),
        senderActions: bindActionCreators(senderActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
