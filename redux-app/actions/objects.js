import store from '../store';
import {httpAsyncApi} from '../lib/http'

// получаем список объектов по данному фильтру
export function get_objects(filter_params){
	return	(dispatch) => {
        $('#loader').show();
        let resultRequest = httpAsyncApi('GET_OBJECTS', filter_params);
        if('then' in resultRequest) {
            resultRequest.then(
                response_data => {
                    let objects = response_data.objects;
                    let objects_all = objects.length;
                    dispatch({
                        type: 'GET_LIST_OBJECT_ALL',
                        payload: objects_all
                    });
                    dispatch({
                        type:	'GET_LIST_OBJECT_ON_PAGE',
                        payload:	objects
                    });
                    $('#loader').hide();
                },
                error_text => {
                    console.error(error_text);
                }
            );
        }
        else {
            console.log('redux-action get_objects, request not promise !');
        }
    }
}

export function display_objects(arr_objects) {
    return {
        type: 'GET_LIST_OBJECT_ON_PAGE',
        payload: arr_objects
    };
}

// если фильтр не изменился, а он и не должен был измениться, берём следующую страницу
export function get_objects_other_page(num_page, on_page) {
    return {
        type: 'SET_PAGE',
        payload: num_page
    };
}

// изменияем количество объектов на странице
export function change_quantity_on_page(on_page) {
    return {
        type: 'SET_OBJECTS_ON_PAGE',
        payload: on_page
    };
}

// добавляем/удаляем в store.objects_choice ещё объект
export function addremove_object_to_choices(object_id){
    return (dispatch, getState) => {
        let objs_choice = getState().objects_choice.ids;
        if(objs_choice.indexOf(object_id) > -1) {
            objs_choice = objs_choice.filter(function(v){ return v != object_id; });
        }
        else {
            objs_choice.push(object_id);
        }
        dispatch({
            type: 'OBJECT_CHOICE_ADD_OBJECTS',
            payload: objs_choice
        });
    }
}

// добавляем несколько объектов в store.objects_choice
export function add_several_objects_to_choices(arr_objects_ids){
    let objs_choice = store.getState().objects_choice.ids;
    return {
        type: 'OBJECT_CHOICE_ADD_OBJECTS',
        payload: [ ...new Set([...objs_choice,...arr_objects_ids])]
    };
}

export function clear_object_choices() {
    return {
        type: 'OBJECT_CHOICE_CLEAR_OBJECTS',
    }
}

export function addrm_objects_to_choices(objects_ids, action=true) { // action: true - add, false - remove
    return (dispatch, getState) => {
        let objs_choice = getState().objects_choice.ids;
        // console.log('action:' + action);
        if(action) {
            objs_choice = [ ...new Set([...objs_choice,...objects_ids])]; // ES 6 drive !!!
        }
        else {
            objs_choice = objs_choice.filter(function(v){ return objects_ids.indexOf(v) < 0; });
        }
        console.log('objs_choice result:');
        console.log(objs_choice);
        dispatch({
            type: 'OBJECT_CHOICE_ADD_OBJECTS',
            payload: objs_choice
        });
    }
}

export function get_cooridate_object_by_address(obj_id, address) {
    return (dispatch) => {
        let data = {'address': address, 'object_id': obj_id};
        let resultRequest = httpAsyncApi('GET_COORDINATE_OBJECTS', data);
        if('then' in resultRequest) {
            resultRequest.then(
                response_data => {
                    try{
                        let result = response_data.result;
                        console.log(result);
                        let text = 'Координаты: ' ;
                        if(result) {
                            text += result;
                            text += '<br><h3>Объекты рядом:</h3><div class="row">';
                            let near = response_data.near;
                            if(near) {
                                for(var i=0, len=near.length; i<len; i++) {
                                    text += '<div class="near_element"><a href="' + near[i]['href'] + '" target="_blank">' + near[i]['address'] + ', ' + near[i]['name'] + '</a></div>';
                                }
                            }
                            text += '</div>';
                        } else {
                            text += ' не определены из-за не точности адреса';
                        }

                        $('#modal_body_text').html(text);
                        $('#object_modal').modal('show');
                    } catch(e) {
                        console.error('actions get_cooridate_object_by_address:' + e);
                    }
                },
                error_text => {
                    console.error(error_text);
                }
            );
        }
        else {
            console.log('redux-action get_coordinate_objects, request not promise !');
        }
    }
}
