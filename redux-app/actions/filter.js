import store from '../store';
import * as obj_actions from './objects';

// добавляем несколько объектов в store.objects_choice
export function filtering(filtering_params){
    return obj_actions.get_objects(filtering_params);
}

export function clear_object_choices() {
    return {
        type: 'FILTER_SUBMIT_FORM',
    }
}
