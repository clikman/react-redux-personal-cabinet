import store from '../store';

// получаем список объектов по данному фильтру
export function get_objects(filter_params){

	return	(dispatch) => {
        dispatch({
            type: 'GET_LIST_OBJECT_ALL',
            payload: objects_all
        });
        dispatch({
            type:	'GET_LIST_OBJECT_ON_PAGE',
            payload:	objects
        });
    }
}

// если фильтр не изменился, а он и не должен был измениться, берём следующую страницу
export function clear_objects_choice() {
    return {
        type: 'OBJECT_CHOICE_CLEAR_OBJECTS'
    }
}

// добавляем в store.objects_choice ещё объект
export function add_object_to_choices(object_id){
    return (dispatch, getState) => {
        let objs_choice = getState().objects_choice;
        objs_choice.push(object_id);
        dispatch({
            type: 'OBJECT_CHOICE_ADD_OBJECTS',
            payload: objects_choice
        });
    }
}
