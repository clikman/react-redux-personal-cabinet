'use strict';

let urls = {
    GET_OBJECTS: '/rest/api/filtering',
    GET_COORDINATE_OBJECTS: '/rest/api/get_coordinate',
}

/** принимаем или действие или урл с данными и отдаем Promise объект
 *
 */
export function httpAsyncApi(what_do, data) {
    let path_url = '';
    if(what_do in urls){
        path_url = urls[what_do];
    }
    else {
        path_url = what_do;
    }
    return httpAsync(path_url, data);
}

function httpAsync(path_url, data) {
    return new Promise(function(resolve, reject) {
        var ajax = $.ajax({
                url: path_url,
                type: 'post',
                async: true,
                data: data
            })
            .done(function(data) {
                resolve(data)
            })
            .fail(function(jqXHR, textStatus) {
                reject( "Respors error:" + textStatus );
            });
            // .always(function() {
            //     log("complete");
            // });
    });
}
