
import React from 'react';
import PropTypes from 'prop-types';
import {Collapse} from 'react-bootstrap';

export default class Sender extends React.Component {
    static propTypes = {
        objects_selected: React.PropTypes.array,
        sender_actions: React.PropTypes.object,
        object_actions: React.PropTypes.object,
    };
    
    static defaultProps = {
        objects_selected: [],
    };
    
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = {
            detailed: false
        };
    }
    toggle() {
      	console.log(this.state.detailed);
        $('#sender_box').toggle();
    }

    render() {
        const {objects_selected, sender_actions, object_actions} = this.props;
        return <div className="position_right_bottom panel panel-info bg-info" id="sender_box">
                <div className="panel-heading" onClick={this.toggle}>Выбранные объекты <span id="sender_box_collapse" className="glyphicon glyphicon-menu-right" aria-hidden="true"></span></div>
                <div className="panel-body">
                    <button className="btn" onClick={sender_actions.clear_objects_choice} >Очистить</button>
                    <p className="selected_elems">Выбранный объектов {objects_selected.length}</p>
                </div>
        </div>;
    }
}
