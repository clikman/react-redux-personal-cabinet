import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';

const renderField = ({ input, label, type, meta: { touched, error } }) => (
  <div>
    <label>{label}</label>
    <div>
      <input {...input} placeholder={label} type={type}/>
      {touched && error && <span>{error}</span>}
    </div>
  </div>
)

const renderCheckboxGroup = ({ name, options,  input, meta, ...custom}) => {
  let $options = options.map((option, i) => (
    <div key={i}>
      <Field
        name={`${name}[${i}]`}
        defaultChecked={input.value.indexOf(option.value) !== -1}
        label={option.label}
        onCheck={(e, checked) => {
          let newValue = [...input.value];
          if (checked){
            newValue.push(option.value);
          } else {
            newValue.splice(newValue.indexOf(option.value), 1);
          }
          return input.onChange(newValue);
        }}
        {...custom}
      />
    </div>
  ));
  return (
    <div>
      {$options}
    </div>
  );
};

const renderFieldRange = ({ input, label, type, meta: { touched, error } }) => (
  <div>
    <div>
      <input {...input} placeholder={label} type={type}/>
      {touched && error && <span>{error}</span>}
    </div>
  </div>
)


class CheckboxGroup extends React.Component {
    checkboxGroup() {
        let {label, required, options, input, meta} = this.props;

        return options.map((option, index) => {
            return (
            <div className="checkbox" key={index}>
                <div>
                <input type="checkbox"
                    name={`${input.name}[${index}]`}
                    value={option.id}
                    checked={input.value.indexOf(option.id) !== -1}
                    onChange={(event) => {
                        const newValue = [...input.value];
                        if (event.target.checked) {
                            newValue.push(option.id);
                        } else {
                            newValue.splice(newValue.indexOf(option.id), 1);
                        }
                       return input.onChange(newValue);
                    }}
                    id={`${input.name}_${option.id}`}
                />
                <label htmlFor={`${input.name}_${option.id}`}>{option.name}</label>
                </div>
            </div>)
        });
    }

    render() {
        return (
            <div>
                {this.checkboxGroup()}
            </div>
        )
    }
}

export class FilterForm extends React.Component {

    render() {
        const { handleSubmit, pristine, reset, submitting } = this.props;
        const optionsList = [{id: 1, name: 'Квартира'}, {id: 2, name: 'Комната'}, {id: 3, name: 'Дом'}]
        return (
            <form onSubmit={handleSubmit}>
                <div className="row">
                    <div className="col-md-6 form-group">
                        <label>Регион</label>
                        <div>
                            <Field name="region" component="select" className="form-control" multiple>
                                <option value="">Все</option>
                                <option value="46">Курская обл</option>
                                <option value="40">Калужская обл</option>
                                <option value="42">Кемеровская обл</option>
                                <option value="22">Алтайский край</option>
                            </Field>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="form-group">
                            <label>Цена</label>
                            <div className="row">
                                <div className="col-md-6">
                                    <Field name="price_from" component={renderFieldRange} type="number" label="от" />
                                </div>
                                <div className="col-md-6">
                                    <Field name="price_to" component={renderFieldRange} type="number" label="до" />
                                </div>
                            </div>
                        </div>
                        <div className="filter_checkboxes">
                            <label className="clr" htmlFor="type_object">Тип объекта</label>
                            <Field name="type_realty" component={CheckboxGroup} options={optionsList} />
                        </div>
                        <div className="btn-group" role="group" aria-label="От кого объявление">
                            <label>От кого объявление</label>
                            <div>
                                <Field name="owner_ad" component="select" className="form-control">
                                    <option value="">Все</option>
                                    <option value="0">Не определено</option>
                                    <option value="1">Собственник</option>
                                    <option value="2">Агентство/риэлтор</option>
                                </Field>
                            </div>
                        </div>
                    </div>
                </div>

                <div  className="">
                    <button type="button" onClick={reset} disabled={pristine || submitting} className="btn btn-info btn-lg" >Очистить</button>{/* btn-default */}
                    <button type="submit" className="btn btn-success btn-lg ml-15" >Фильтровать</button>
                </div>
            </form>
        );
    }
}

const filtering_validate_func = values => {
    const errors = {}
    if (!values.username) {
        errors.username = 'Обязательное поле'
    } else if (values.username.length > 15) {
        errors.username = 'Must be 15 characters or less'
    }
    else if(values.username.length < 3) {
          errors.username = 'Слишком короткое имя у Вас';
    }
    if (!values.email) {
        errors.email = 'Обязательное поле'
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
        errors.email = 'Invalid email address'
    }
    if (!values.age) {
        errors.age = 'Обязательное поле'
    } else if (isNaN(Number(values.age))) {
        errors.age = 'Must be a number'
    } else if (Number(values.age) < 18) {
        errors.age = 'Sorry, you must be at least 18 years old'
    }

    if(values.price_from && values.price_to && values.price_from > values.price_to) {
        errors.price_from = 'Цена в поле "от" не должна быть больше цены в поле "до" !';
    }
    return errors
}

// Decorate the form component
FilterForm = reduxForm({
    form: 'filtering', // a unique name for this form
    validate: filtering_validate_func
})(FilterForm);

export default FilterForm;
