
import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

import Page from './pagination'

class ObjectsPage extends React.Component {
    static propTypes = {
        objects: React.PropTypes.array,
        objects_all: React.PropTypes.number,
        objects_actions: React.PropTypes.object,
        objects_prop: React.PropTypes.object,
        page: React.PropTypes.number,
        on_page: React.PropTypes.number,
        objects_selected: React.PropTypes.array,
    };
    
    static defaultProps = {
        objects: [],
        objects_all: 0,
        page: 1,
        on_page: 50,
    };
    
    state = {
        page: this.props.page,
        pages: 1,
        objects_display: [],
    }

    componentWillReceiveProps(nextProps){
        if(this.props.objects_all > 0) {
            var pages_get = nextProps.objects_all ? Math.ceil(nextProps.objects_all / nextProps.on_page) : 0;
        }
        else {
            var pages_get = 0;
        }
        let objects_dis = this.getObjectsDisplay(nextProps.objects_prop.objects, nextProps.objects_prop.page, nextProps.objects_prop.on_page);
        this.state = {
            pages: pages_get,
            page: parseInt(nextProps.objects_prop.page),
            objects_display: objects_dis
        }
    }
    
    getObjectsDisplay(objects, page=1, on_page=50) {
        if(!objects)
            objects = this.props.objects;
        let begin = (page - 1) * on_page;
        let end = page * on_page;
        return objects.slice(begin, end);
    }

    render() {
        const {objects_all, objects, objects_actions, objects_selected, on_page} = this.props;
        return <div>
            <div className="well well-sm">Всего объектов: {objects_all}</div>
            <Objects
                objects={this.state.objects_display}
                objects_list={objects}
                add_to={objects_actions.addremove_object_to_choices}
                get_geo={objects_actions.get_cooridate_object_by_address}
                add_to_group={objects_actions.addrm_objects_to_choices}
                selected={objects_selected}
                display_objects={objects_actions.display_objects}
                page={this.state.page}
                on_page={on_page}
            />
            <Page pages={this.state.pages} page={this.state.page} on_page={on_page} action={objects_actions.get_objects_other_page}/>
        </div>;
    }
}

export default ObjectsPage;

/**  получает список объектов и отображает их в обозначеном виде
*
*/
class Objects extends React.Component {
    static propTypes = {
        objects: React.PropTypes.array,
        objects_list: React.PropTypes.array,
        selected: React.PropTypes.array,
        add_to: React.PropTypes.func,
        display_objects: React.PropTypes.func,
        page: React.PropTypes.number,
        on_page: React.PropTypes.number,
    }
    
    constructor(props) {
        super(props);
    }
    
    checking(e) {
        let targ = e.target;
        this.props.add_to(targ.value);
    }
    
    checkingAll(e) {
        try {
            let checkbox_all = e.target;
            if(checkbox_all) {
                let ids_arr = [];
                let action = false;
                if(checkbox_all.checked == true) {
                    action = true;
                }
                else {
                    // console.log('click all checked - FLUSH');
                }
                try {
                    let chkbx = $('.chkbx');
                    for(var k=0, len=chkbx.length; k<len; k++){
                        ids_arr.push(chkbx[k].value);
                    }
                    this.props.add_to_group(ids_arr, action);
                }
                catch(err) {
                    console.error('Ошибка при поиске объектов(chekbox) на добавление: ' + err);
                }
            }
        }
        catch(err) {
            console.error("Не найден checkbox для добавление в обработчике" + err);
        }
    }
    
    getGeo(e) {
        let targ = e.target;
        let addr = targ.getAttribute('data-address')
        let id = targ.getAttribute('data-value')
        this.props.get_geo(id, addr);
    }
    
    sort(ev, current, field) {
        $('.arrow_up_down, .arrow_up, .arrow_down').removeClass('active');
        let elem = ev.target;
        elem.classList.add("active");
        let objs = this.props.objects_list;
        try {
            let sort_field_values = [];
            let sort_list = [];
            switch(current) {
                case 'up':
                    sort_list = objs.sort(function(a,b) {return (a[field] > b[field]) ? 1 : ((b[field] > a[field]) ? -1 : 0);} );
                    break;
                case 'down':
                    sort_list =  objs.sort(function(a,b) {return (a[field] < b[field]) ? 1 : ((b[field] < a[field]) ? -1 : 0);} );
                    break;
                default:
                    sort_list = objs.sort(function(a,b) {return (a[field] == b[field]) ? 1 : ((b[field] > a[field]) ? -1 : 0);} );
                    break;
            }
            this.props.display_objects(sort_list);
        }
        catch(err) {
            console.error(err);
        }
    }

    render(){
        let objs = this.props.objects;
        let selected = this.props.selected;
        let page = parseInt(this.props.page);
        let on_page = parseInt(this.props.on_page);
        var that = this;
        if(objs.length && selected) {
            var	objectTemplate	=	objs.map(function(item,	index)	{
                return	(
                    <tr key={index} className="obj_elem">
                        <td>{index + 1 + on_page * (page - 1)}</td>
                        <td><input type="checkbox" id={"object_" + item.id } value={item.id} onChange={::that.checking} className='chkbx' checked={selected.indexOf(""+item.id) > -1 ? 'checked' : false} /></td>
                        <td>
                            {
                                item.href
                                ?
                                <a href={item.href} target="_blank" ><h4>{item.name}</h4></a>
                                :
                                <h4>{item.name}</h4>
                            }   
                            {
                                item.contacts_src && item.contacts_src.length > 5
                                ?
                                    <div>
                                        <button data-toggle="collapse" data-target={"#collap_"+item.id} className="btn">Контакты</button>
                                        <div id={"collap_"+item.id} className="collapse">
                                                Адрес: {item.address}<br/>
                                                Контактное лицо: {item.contacts}<br/>
                                                Телефон: <img src={item.contacts_src} />
                                        </div>
                                    </div>
                                :
                                ''
                            }
                        </td>
                        <td className="object_description">
                            <span className="object_price">{item.price}</span>
                        </td>
                        <td>
                            {
                                item.src ?
                                ''
                                :
                                ''
                            }
                        </td>
                        <td>
                            {item.region ? item.region : '-'}
                        </td>
                        <td>
                        </td>
                        <td>
                            {parseInt(item.owner_ad) ? (item.owner_ad == 1 ? 'собств.' : 'агент.') : ''}
                        </td>
                        <td>
                            {
                                item.contacts_src && item.contacts_src.length > 5
                                ?
                                <img src="/static/images/icons/geo.png" className="geo-label" onClick={::that.getGeo} data-value={item.id} data-address={item.subregion + ', ' + item.address} />
                                :
                                ''
                            }
                        </td>
                    </tr>
                    )
                });
        }
        else {
            objectTemplate = <tr><td rowSpan="5">Объектов нет</td></tr>
        }
        return <div className="objects">
            <table className="table">
                <thead>
                    <tr>
                        <th>№</th>
                        <th>Выбрать</th>
                        <th>Объект</th>
                        <th>Цена</th>
                        <th>Изображение</th>
                        <th>Регион</th>
                        <th>Адрес</th>
                        <th>Доп.инфо</th>
                        <th>i</th>
                    </tr>
                    {
                        objs.length ?
                        <tr>
                            <th></th>
                            <th><input type="checkbox" id="cheching_object_all" onChange={::that.checkingAll} ref='chkbx_all' /></th>
                            <th>
                                <span className="arrow_up" onClick={(e) => this.sort(e, 'up', 'name')} >&uarr;</span>
                                <span className="arrow_down" onClick={(e) => this.sort(e, 'down', 'name')}>&darr;</span>
                            </th>
                            <th>
                                <span className="arrow_up"  onClick={(e) => this.sort(e, 'up', 'price')} >&uarr;</span>
                                <span className="arrow_down"  onClick={(e) => this.sort(e, 'down', 'price')}>&darr;</span>
                            </th>
                            <th></th>
                            <th></th>
                            <th>
                                <span className="arrow_up"  onClick={(e) => this.sort(e, 'up', 'address')} >&uarr;</span>
                                <span className="arrow_down"  onClick={(e) => this.sort(e, 'down', 'address')}>&darr;</span>
                            </th>
                            <th></th>
                        </tr>
                        : ''
                    }
                </thead>
                <tbody>
                    {objectTemplate}
                </tbody>
            </table>
            <div id="object_modal" className="modal fade" role="dialog">
                <div className="modal-dialog">
                    <div className="modal-content">
                      <div className="modal-header">
                        <button type="button" className="close" data-dismiss="modal">&times;</button>
                        <h4 className="modal-title">Объекты рядом</h4>
                      </div>
                      <div className="modal-body" id="modal_body_text">
                        <p>Some text in the modal.</p>
                      </div>
                      <div className="modal-footer">
                        <button type="button" className="btn btn-default" data-dismiss="modal">Закрыть</button>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    }
}



class OnPageSelector extends React.Component {
    static propTypes = {
        variants: React.PropTypes.array
    }
    static defaultProps = {
        variants: [20, 50, 100, 200]
    }
    constructor(props) {
        super(props);
        this.state = {
            value: 0
        }
    }
}
