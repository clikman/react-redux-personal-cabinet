
import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

/** компонент должен отрисовать блок пагинации, получив через свойства: сколько страниц он должен отобразить, событие при нажатии на номер страницы
* событие передаваемое в компонент должно принимать номер страницы на которой кликнули
*/
export default  class Pages extends React.Component {
    static propTypes = {
        action: React.PropTypes.func,
        on_page: React.PropTypes.number,
        pages: React.PropTypes.number,
        page: React.PropTypes.number,
    }
    constructor(props) {
        super(props);
        this.changeP = this.changePage.bind(this);
    }
    changePage(ev) {
        let elem = ev.target.textContent;
        console.log('click on page: ' + elem);
        this.props.action(elem, this.props.on_page); // on_page не обязательно
    }

    render() {
        const {on_page, pages, page} = this.props;
        var that = this;
        var paginationList = [...Array(pages + 1).keys()].map(function(item, index) {
            if(index > 0) {
                return (
                    <li key={index} className={page==index ? 'active' : ''} onClick={(e) => that.changePage(e, page)}>
                        <span aria-hidden="true">
                            <a aria-label>{item}</a>
                        </span>
                    </li>
                )
            }
        });
        return <div className="well well-sm">
            <nav aria-label="Постраничная навигация">
                    {
                        pages
                        ?
                        <ul className="pagination">
                            {paginationList}
                        </ul>
                        :
                        ''
                    }
            </nav>
            <p className="paginator_all_pages">
                Всего страниц: {pages} { on_page ? <span>на странице {on_page} </span> : '' }
            </p>
        </div>
    }
}
