
import React from 'react';
import PropTypes from 'prop-types';

import FilterForm from './filter_form'

export default class FilterFormContainer extends React.Component {
    static propTypes = {
        filter_selected: React.PropTypes.array,
        filter_actions: React.PropTypes.object
    };
    
    static defaultProps = {
        filter_selected: [],
    };
    
    state = {
        selected: []
    }
    
    submit = (values) => {
        let len = [];
        let val = {};
        if(values) {
            for(let [k, v] of Object.entries(values)) {
                let new_value = v;
                if(typeof v != 'object') {
                    len.push(k);
                }
                else if( !(v instanceof Array) ) {
                    let new_array = [];
                    for(let [kk,vv] of Object.entries(v)) {
                        if(vv) {
                            len.push(k);
                            new_array.push(kk);
                        }
                    }
                    new_value = new_array;
                }
                val[k] = new_value;
            }
        }
        this.setState({selected: len});
        this.props.filter_actions.filtering(val);
    }
    
    render() {
        const {filter_selected, filter_actions} = this.props;
        return (
            <div className="panel panel-default">
                <div className="panel-heading">Фильтр</div>
                <div className="panel-body">
                    <FilterForm onSubmit={this.submit} />
                </div>
                <div className="panel-footer">
                    <p className="">Параметров фильтрации: {this.state.selected.length}</p>
                    <div className="loader" id="loader"></div>
                </div>
            </div>
        )
    }
}
