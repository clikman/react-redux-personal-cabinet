
import React from 'react';
import PropTypes from 'prop-types';

export default class User extends React.Component {
    static propTypes = {
        name: React.PropTypes.string,
        user_actions: React.PropTypes.object,
    };
    
    static defaultProps = {
        name: 'Гость',
    };
    
    constructor(props) {
        super(props);
    }
    render() {
        const {name, user_actions} = this.props;
        return <div>
            <p className="row">Здравствуйте: {name}</p>
            <p><button className="btn">Войти </button></p>
        </div>;
    }
}
