const GET_LIST_OBJECT_ON_PAGE = 'GET_LIST_OBJECT_ON_PAGE'
const GET_LIST_OBJECT_ALL = 'GET_LIST_OBJECT_ALL'
const SET_OBJECTS_ON_PAGE = 'SET_OBJECTS_ON_PAGE'
const SET_PAGE = 'SET_PAGE'


const initialState = {
    objects_all: 0,
    objects: [],
    fetching: false
}

export default function objects(state = initialState, action) {

    switch (action.type) {
        case GET_LIST_OBJECT_ON_PAGE:
            return { ...state, objects: action.payload, page: 1, fetching: true }

        case GET_LIST_OBJECT_ALL:
            return { ...state, objects_all: action.payload, fetching: false }

        case SET_OBJECTS_ON_PAGE:
            return {...state, on_page: action.payload, page: 1}

        case SET_PAGE:
            return {...state, page: action.payload}

        default:
            return state;
    }

}
