const SENDER_CLEAR = 'SENDER_CLEAR'
const SENDER_SEND_CHOICED_OBJECTS = 'SENDER_SEND_CHOICED_OBJECTS'


const initialState = {
    emails: [], // emails: кому отправить
    send: false
}

export default function sender(state = initialState, action) {

    switch (action.type) {
        case SENDER_CLEAR:
          return { ...state, send: false }

        case SENDER_SEND_CHOICED_OBJECTS:
            return { ...state, send: true, emails: initialState.emails }

        default:
            return state;
    }

}
