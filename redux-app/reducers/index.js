import { combineReducers } from 'redux'
import filter from './filter'
import user from './user'
import sender from './sender'
import objects from './objects'
import objects_choice from './objects_choice'
import { reducer as form_reducer } from 'redux-form'


export default combineReducers({
    filter,
    user,
    sender,
    objects,
    objects_choice,
    form: form_reducer
})
