const FILTER_SUBMIT_FORM = 'FILTER_SUBMIT_FORM'
const FILTER_VALID = 'FILTER_VALID'


const initialState = {
    objects_all: 0,
    objects: [],
    fetching: false,
    filter_selected: []
}

export default function filter(state = initialState, action) {

  switch (action.type) {
    case FILTER_SUBMIT_FORM:
      return { ...state, filter_selected: action.payload, fetching: true }

    case FILTER_VALID:
      return { ...state, filter_selected: action.payload, fetching: false }

    default:
      return state;
  }

}
