const USER_LOGIN = 'USER_LOGIN'
const USER_LOGOUT = 'USER_LOGOUT'


const initialState = {
    name: 'Аноним',
    auth: false
}

export default function user(state = initialState, action) {

  switch (action.type) {
    case USER_LOGIN:
      return { ...state, name: action.payload, auth: true }

    case USER_LOGOUT:
      return { ...state, name: initialState.user, auth: false }

    default:
      return state;
  }

}
