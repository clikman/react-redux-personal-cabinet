
const OBJECT_CHOICE_ADD_OBJECTS = 'OBJECT_CHOICE_ADD_OBJECTS'
const OBJECT_CHOICE_CLEAR_OBJECTS = 'OBJECT_CHOICE_CLEAR_OBJECTS'


const initialState = {
    objects: [],
    ids: [],
}

export default function objects_choice(state = initialState, action) {

  switch (action.type) {
    case OBJECT_CHOICE_ADD_OBJECTS:
        return { ...state, ids: action.payload }

    case OBJECT_CHOICE_CLEAR_OBJECTS:
        return { ...state, ids: [] }

    default:
        return state;
  }

}
